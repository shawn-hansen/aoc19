package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {
	orbitMap := make(map[string]string)
	file, _ := os.Open("inputs/day6.input")
	defer file.Close()

	scanner := bufio.NewScanner(file)

	for scanner.Scan() {
		orbit := strings.Split(scanner.Text(), ")")
		orbitMap[orbit[1]] = orbit[0]
	}

	var myAncestory []string
	var santasAncesory []string

	for child, _ := range orbitMap {
		if child == "YOU" {
			myAncestory = getAncestors(child, orbitMap)
		} else if child == "SAN" {
			santasAncesory = getAncestors(child, orbitMap)
		}
	}

	cnt := countNotSharedAncestry(myAncestory, santasAncesory)
	cnt += countNotSharedAncestry(santasAncesory, myAncestory)

	fmt.Println(santasAncesory)
	fmt.Println(myAncestory)

	fmt.Println(cnt)
}

func getAncestors(child string, orbits map[string]string) []string {
	parent := orbits[child]
	var ancestors []string
	ancestors = append(ancestors, parent)

	if parent == "COM" {
		return ancestors
	}

	return append(ancestors, getAncestors(parent, orbits)...)
}

func countNotSharedAncestry(ancestry1 []string, ancestry2 []string) int {
	var cnt int
	for _, a1 := range ancestry1 {
		if indexOf(ancestry2, a1) < 0 {
			cnt++
		}
	}

	return cnt
}

func indexOf(slice []string, value string) int {
	for i, v := range slice {
		if v == value {
			return i
		}
	}
	return -1
}