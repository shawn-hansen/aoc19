package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	file, _ := os.Open("inputs/day1a.input")
	defer file.Close()
	var totalFuel int
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		moduleMass, _ := strconv.Atoi(scanner.Text())
		totalFuel += getFuelAmount(moduleMass)
	}
	fmt.Println(totalFuel)
}

func getFuelAmount(mass int) int {
	fuelNeeded := (mass / 3) - 2
	if fuelNeeded <= 0 {
		return 0
	}
	fuelNeeded += getFuelAmount(fuelNeeded)
	return fuelNeeded
}
