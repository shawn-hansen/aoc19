package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

type Op struct {
	opCode     int
	param1Mode int
	param2Mode int
	param3Mode int
}

func main() {
	bytes, _ := ioutil.ReadFile("inputs/day5.input")

	ints := getInts(strings.Split(string(bytes), ","))
	processOps(ints, 5)
}

func processOps(ints []int, userInput int) {
	var currentOpPos int

	for {
		var (
			parameter1 int
			parameter2 int
			parameter3 int
			valCalc    int
		)

		op := parseOpcode(ints[currentOpPos])
		if op.opCode == 99 {
			break
		}

		//parameter3 = ints[currentOpPos+3]

		switch op.opCode {
		case 1:
			parameter1 = getParameter(ints, 1, op.param1Mode, currentOpPos)
			parameter2 = getParameter(ints, 2, op.param2Mode, currentOpPos)
			parameter3 = getParameter(ints, 3, 1, currentOpPos)
			valCalc = parameter1 + parameter2
			ints[parameter3] = valCalc
			currentOpPos += 4
		case 2:
			parameter1 = getParameter(ints, 1, op.param1Mode, currentOpPos)
			parameter2 = getParameter(ints, 2, op.param2Mode, currentOpPos)
			parameter3 = getParameter(ints, 3, 1, currentOpPos)
			valCalc = parameter1 * parameter2
			ints[parameter3] = valCalc
			currentOpPos += 4
		case 3:
			parameter1 = getParameter(ints, 1, 1, currentOpPos)
			valCalc = userInput
			ints[parameter1] = valCalc
			currentOpPos += 2
		case 4:
			parameter1 = getParameter(ints, 1, op.param1Mode, currentOpPos)
			fmt.Println(parameter1)
			currentOpPos += 2
		case 5:
			parameter1 = getParameter(ints, 1, op.param1Mode, currentOpPos)
			parameter2 = getParameter(ints, 2, op.param2Mode, currentOpPos)
			if parameter1 != 0 {
				currentOpPos = parameter2
			} else {
				currentOpPos += 3
			}
		case 6:
			parameter1 = getParameter(ints, 1, op.param1Mode, currentOpPos)
			parameter2 = getParameter(ints, 2, op.param2Mode, currentOpPos)
			if parameter1 == 0 {
				currentOpPos = parameter2
			} else {
				currentOpPos += 3
			}
		case 7:
			parameter1 = getParameter(ints, 1, op.param1Mode, currentOpPos)
			parameter2 = getParameter(ints, 2, op.param2Mode, currentOpPos)
			parameter3 = getParameter(ints, 3, 1, currentOpPos)
			if parameter1 < parameter2 {
				ints[parameter3] = 1
			} else {
				ints[parameter3] = 0
			}
			currentOpPos += 4
		case 8:
			parameter1 = getParameter(ints, 1, op.param1Mode, currentOpPos)
			parameter2 = getParameter(ints, 2, op.param2Mode, currentOpPos)
			parameter3 = getParameter(ints, 3, 1, currentOpPos)
			if parameter1 == parameter2 {
				ints[parameter3] = 1
			} else {
				ints[parameter3] = 0
			}
			currentOpPos += 4
		}
	}
}

func getParameter(ints []int, parameter int, parameterMode int, offset int) int {
	if parameterMode == 1 {
		return ints[offset+parameter]
	} else {
		return ints[ints[offset+parameter]]
	}
}

func getInts(strings []string) []int {
	var ints []int

	for _, i := range strings {
		j, err := strconv.Atoi(i)
		if err != nil {
			panic(err)
		}
		ints = append(ints, j)
	}
	return ints
}

func parseOpcode(opCode int) Op {
	return Op{
		opCode % 100,
		(opCode / 100) % 10,
		(opCode / 1000) % 10,
		(opCode / 10000) % 10,
	}
}
