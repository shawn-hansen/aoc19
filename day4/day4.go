package main

import (
	"fmt"
	"sort"
)

//240298-784956
func main() {
	i := 240298
	cnt := 0
	for i <= 784956 {
		if checkIncreasing(i) && checkDouble(i) {
			cnt++
		}
		i++
	}
	fmt.Print(cnt)
}

func checkIncreasing(num int) bool {
	return sort.IntsAreSorted(getDigitSlice(num))
}

func checkDouble(num int) bool {
	digits := getDigitSlice(num)
	countMap := make(map[int]int)
	for i := 0; i < len(digits); i++ {
		countMap[digits[i]]++
	}
	for _, cnt := range countMap {
		if cnt == 2 {
			return true
		}
	}
	return false
}

func getDigitSlice(num int) []int {
	return []int{
		(num / 100000) % 10,
		(num / 10000) % 10,
		(num / 1000) % 10,
		(num / 100) % 10,
		(num / 10) % 10,
		num % 10,
	}
}
