package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func main() {
	bytes, _ := ioutil.ReadFile("inputs/day2a.input")
	for n := 1; n < 99; n++ {
		for v := 1; v < 99; v++ {
			ints := getInts(strings.Split(string(bytes), ","))
			ints[1] = n
			ints[2] = v
			out := processOps(ints)
			if out == 19690720 {
				fmt.Print((100 * n) + v)
				return
			}
		}
	}
}

func processOps(ints []int) int {
	currentOp := 0
	for {
		op := ints[currentOp]
		if op == 99 {
			return ints[0]
		}
		val1Pos := ints[currentOp+1]
		val2Pos := ints[currentOp+2]
		storePosistion := ints[currentOp+3]
		var valCalc int
		if op == 1 {
			valCalc = ints[val1Pos] + ints[val2Pos]
		} else if op == 2 {
			valCalc = ints[val1Pos] * ints[val2Pos]
		}

		ints[storePosistion] = valCalc
		currentOp += 4
	}

}

func getInts(strings []string) []int {
	var ints = []int{}

	for _, i := range strings {
		j, err := strconv.Atoi(i)
		if err != nil {
			panic(err)
		}
		ints = append(ints, j)
	}
	return ints
}
