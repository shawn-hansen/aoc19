package main

import (
	"fmt"
	"sync"
)

var wg sync.WaitGroup
func main() {
	chan1 := make(chan int)
	chan2 := make(chan int)
	chan3 := make(chan int)
	chan4 := make(chan int)

	//doneChan := make(chan bool)

	wg.Add(3)
	go pipe(chan1, chan2, "1->2")//, doneChan)
	go pipe(chan2, chan3, "2->3")//, doneChan)
	go pipe(chan3, chan4, "3->4")//, doneChan)
	go pipe(chan4, chan1, "4->1")//, doneChan)

	chan1 <- 1

	//<-doneChan
	//<-doneChan
	//<-doneChan
	//<-doneChan

	wg.Wait()
	wg.Add(1)
	fmt.Printf("final msg %d\n", <-chan1)
}

func pipe(inChannel chan int, outChannel chan int, label string){//}, done chan bool) {

	for i := 0; i < 5; i++ {
		//fmt.Printf("pipe loop %d\n", i)
		a := <-inChannel
		fmt.Printf("%s %d\n", label, a)
		outChannel <- a + 1
	}
	//done <- true
	fmt.Println(label + " done")
	wg.Done()
}