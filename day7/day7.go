package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
	"sync"
)

type Op struct {
	opCode     int
	param1Mode int
	param2Mode int
	param3Mode int
}

var wg sync.WaitGroup

func main() {
	bytes, _ := ioutil.ReadFile("inputs/day7.test.txt")

	ints := getInts(strings.Split(string(bytes), ","))

	var thrust int
	var maxThrust int
	//phases := getAllPhaseCombos()
	// for all phase combos
	//for _, p := range phases {
	signalAChan := make(chan int)
	signalBChan := make(chan int)
	signalCChan := make(chan int)
	signalDChan := make(chan int)
	signalEChan := make(chan int)

	//	fmt.Println(p)
	//for _, i := range p {
	wg.Add(4)


	go func() {
		amplify(ints, 9, signalAChan, signalBChan)
		wg.Done()
	}()
	go amplify(ints, 8, signalBChan, signalCChan)
	go amplify(ints, 7, signalCChan, signalDChan)
	go amplify(ints, 6, signalDChan, signalEChan)
	go amplify(ints, 5, signalEChan, signalAChan)

	signalAChan <-0

	wg.Wait()

	thrust = <-signalAChan
	//}
	if thrust > maxThrust {
		maxThrust = thrust
	}
	//}
	fmt.Println(maxThrust)
}

func getAllPhaseCombos() [][]int {
	var phases [][]int
	for i := 5; i <= 9; i++ {
		for j := 5; j <= 9; j++ {
			for k := 5; k <= 9; k++ {
				for l := 5; l <= 9; l++ {
					for m := 5; m <= 9; m++ {
						phase := []int{i, j, k, l, m}
						if containsUnique(phase) {
							phases = append(phases, phase)
						}
					}
				}
			}
		}
	}
	return phases
}

func containsUnique(p []int) bool {
	intMap := make(map[int]bool)
	for _, i := range p {
		intMap[i] = true
	}

	return len(intMap) == 5
}

func amplify(ints []int, phaseSetting int, signalInChan chan int, signalOutChan chan int) {
	var currentOpPos int
	firstInput := true

	for {
		var (
			parameter1 int
			parameter2 int
			parameter3 int
			valCalc    int
		)

		op := parseOpcode(ints[currentOpPos])
		if op.opCode == 99 {
			fmt.Println("got 99")
			break
		}

		//parameter3 = ints[currentOpPos+3]

		switch op.opCode {
		case 1:
			parameter1 = getParameter(ints, 1, op.param1Mode, currentOpPos)
			parameter2 = getParameter(ints, 2, op.param2Mode, currentOpPos)
			parameter3 = getParameter(ints, 3, 1, currentOpPos)
			valCalc = parameter1 + parameter2
			ints[parameter3] = valCalc
			currentOpPos += 4
		case 2:
			parameter1 = getParameter(ints, 1, op.param1Mode, currentOpPos)
			parameter2 = getParameter(ints, 2, op.param2Mode, currentOpPos)
			parameter3 = getParameter(ints, 3, 1, currentOpPos)
			valCalc = parameter1 * parameter2
			ints[parameter3] = valCalc
			currentOpPos += 4
		case 3:
			parameter1 = getParameter(ints, 1, 1, currentOpPos)
			if firstInput {
				valCalc = phaseSetting
				firstInput = false
			} else {
				valCalc = <- signalInChan
			}
			ints[parameter1] = valCalc
			currentOpPos += 2
		case 4:
			parameter1 = getParameter(ints, 1, op.param1Mode, currentOpPos)
			signalOutChan <- parameter1
		case 5:
			parameter1 = getParameter(ints, 1, op.param1Mode, currentOpPos)
			parameter2 = getParameter(ints, 2, op.param2Mode, currentOpPos)
			if parameter1 != 0 {
				currentOpPos = parameter2
			} else {
				currentOpPos += 3
			}
		case 6:
			parameter1 = getParameter(ints, 1, op.param1Mode, currentOpPos)
			parameter2 = getParameter(ints, 2, op.param2Mode, currentOpPos)
			if parameter1 == 0 {
				currentOpPos = parameter2
			} else {
				currentOpPos += 3
			}
		case 7:
			parameter1 = getParameter(ints, 1, op.param1Mode, currentOpPos)
			parameter2 = getParameter(ints, 2, op.param2Mode, currentOpPos)
			parameter3 = getParameter(ints, 3, 1, currentOpPos)
			if parameter1 < parameter2 {
				ints[parameter3] = 1
			} else {
				ints[parameter3] = 0
			}
			currentOpPos += 4
		case 8:
			parameter1 = getParameter(ints, 1, op.param1Mode, currentOpPos)
			parameter2 = getParameter(ints, 2, op.param2Mode, currentOpPos)
			parameter3 = getParameter(ints, 3, 1, currentOpPos)
			if parameter1 == parameter2 {
				ints[parameter3] = 1
			} else {
				ints[parameter3] = 0
			}
			currentOpPos += 4
		}
	}
}

func getParameter(ints []int, parameter int, parameterMode int, offset int) int {
	if parameterMode == 1 {
		return ints[offset+parameter]
	} else {
		return ints[ints[offset+parameter]]
	}
}

func getInts(strings []string) []int {
	var ints []int

	for _, i := range strings {
		j, err := strconv.Atoi(i)
		if err != nil {
			panic(err)
		}
		ints = append(ints, j)
	}
	return ints
}

func parseOpcode(opCode int) Op {
	return Op{
		opCode % 100,
		(opCode / 100) % 10,
		(opCode / 1000) % 10,
		(opCode / 10000) % 10,
	}
}
