package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"strconv"
	"strings"
)

type Point struct {
	X int
	Y int
}

type signal struct {
	strength  int
	frequency int
}

func main() {
	file, _ := os.Open("inputs/day3.input")
	defer file.Close()

	scanner := bufio.NewScanner(file)
	scanner.Scan()
	wire1 := scanner.Text()
	scanner.Scan()
	wire2 := scanner.Text()

	wire1Directions := strings.Split(wire1, ",")
	wire2Directions := strings.Split(wire2, ",")

	// fmt.Printf("Wire1 length: %d, first %s, last %s\n", len(wire1Directions), wire1Directions[0], wire1Directions[len(wire1Directions)-1])
	// fmt.Printf("Wire2 length: %d, first %s, last %s\n", len(wire2Directions), wire2Directions[0], wire2Directions[len(wire2Directions)-1])

	points := make(map[Point]signal)

	mapWire(wire1Directions, points, false)
	mapWire(wire2Directions, points, true)

	closestCrossing := math.MaxInt64
	for crossingPoint, cross := range points {
		if cross.frequency == 3 {
			//fmt.Printf("X: %d	Y: %d	D:%d\n", crossingPoint.X, crossingPoint.Y, cross.strength)

			//distance := math.Abs(float64(crossingPoint.X)) + math.Abs(float64(crossingPoint.Y))
			if crossingPoint.X != 0 && crossingPoint.Y != 0 && cross.strength < closestCrossing {
				closestCrossing = cross.strength
			}
		}
	}
	fmt.Print(closestCrossing)
}

func mapWire(path []string, points map[Point]signal, secondWire bool) {
	var lastPoint Point
	var step int
	for _, vector := range path {
		direction := vector[0]
		distance, _ := strconv.Atoi(vector[1:])
		var currentPoint Point
		for i := 1; i <= distance; i++ {
			step++
			switch direction {
			case 'R':
				currentPoint = makePoint(lastPoint.X+i, lastPoint.Y, points, secondWire, step)
			case 'L':
				currentPoint = makePoint(lastPoint.X-i, lastPoint.Y, points, secondWire, step)
			case 'U':
				currentPoint = makePoint(lastPoint.X, lastPoint.Y+i, points, secondWire, step)
			case 'D':
				currentPoint = makePoint(lastPoint.X, lastPoint.Y-i, points, secondWire, step)
			}
		}

		lastPoint = currentPoint
	}
}

func makePoint(x int, y int, points map[Point]signal, secondWire bool, currentStep int) Point {
	point := Point{x, y}
	if points[point].frequency == 0 && points[point].strength == 0 {

		if secondWire {
			points[point] = signal{currentStep, 2}

		} else {
			points[point] = signal{currentStep, 1}
		}
	} else {
		if secondWire {
			points[point] = signal{points[point].strength + currentStep, 3}
		}
	}

	return point
}
